'use strict'

module.exports = Object.freeze({
  SUPPLEMENTAL_FILES_GLOB: '**/*',
  UI_CACHE_PATH: '.antora-cache/ui',
  UI_CONFIG_FILENAME: 'ui.yml',
})
