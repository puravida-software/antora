= Antora
// Settings:
ifdef::env-gitlab[:outfilesuffix: .adoc]
:badges:
// Project URIs:
:uri-project: https://antora.org
:uri-repo: https://gitlab.com/antora/antora
:uri-issues: {uri-project}/issues
:uri-ci-pipelines: {uri-repo}/pipelines
:img-ci-status: {uri-repo}/badges/master/pipeline.svg
:uri-chat-dev: https://gitter.im/antora/dev
:img-chat-dev: https://img.shields.io/badge/chat-dev-blue.svg
:uri-chat-users: https://gitter.im/antora/users
:img-chat-users: https://img.shields.io/badge/chat-users-blue.svg
:uri-twitter: https://twitter.com/antoraproject
:uri-twitter-hash: https://twitter.com/hashtag/antora?src=hash
// External URIs:
:uri-asciidoctor: https://asciidoctor.org
:uri-choco: https://chocolatey.org
:uri-nvm: https://github.com/creationix/nvm
:uri-nvm-install: {uri-nvm}#installation
:uri-nvm-windows: https://github.com/coreybutler/nvm-windows
:uri-opendevise: https://opendevise.com
// Versions:
:version-node: 8.9.4

ifdef::badges[]
image:{img-ci-status}[CI Status (GitLab CI), link={uri-ci-pipelines}]
image:{img-chat-dev}[Chat - dev (Gitter), link={uri-chat-dev}]
image:{img-chat-users}[Chat - users (Gitter), link={uri-chat-users}]
endif::[]

{uri-project}[Antora] is a modular, multi-repository site generator designed for creating documentation sites from content composed in AsciiDoc and processed with {uri-asciidoctor}[Asciidoctor].

Antora's toolchain and workflow help documentation and engineering teams create, manage, collaborate on, remix, and publish documentation sites sourced from multiple versioned git repositories without needing expertise in web technologies, build automation, or system administration.

This project includes a command-line interface (CLI) and a preassembled site generator pipeline so you can quickly start publishing documentation sites with Antora.

== Quickstart

This section offers a basic tutorial for evaluating Antora.
A more comprehensive tutorial will be in the documentation.

=== Prerequisites

Antora runs on Node 8 (and above).
Antora is verified to work on Linux, macOS, and Windows.

To install Antora, you'll need Node 8 (including npm, which is bundled with Node) on your system.
Unless you're using Windows, you'll also need the base build tools for your OS.

Let's start with the build tools, since they're also useful for managing Node.

==== Base Build Tools

The base build tools are required for installing nodegit, a key dependency of Antora, on Linux and macOS.
If you're running Windows, you can skip this section.

If you're running Linux, select the appropriate base build tools package for your distribution.

* Fedora: `dnf install @development-tools`
* Debian/Ubuntu: `apt-get install build-essential`
* Arch Linux: `pacman -S base-devel`
* Alpine Linux: `apk add build-base libgit2-dev`

If you're running macOS, you need the https://railsapps.github.io/xcode-command-line-tools.html[Xcode command line tools].
Even if you've installed these tools in the past, it's good to do it again.
You can trigger installation using the following command:

 xcode-select --install

This selection of tools provides other useful commands you'll need in your workflow, such as `git`.

Once you have these tools installed, you can expect the remaining installation to go smoothly.

==== Node 8

Antora requires Node 8, the current long-term support (LTS) release of Node.
While you can use Node 9, we don't recommend it because there are currently no precompiled binaries of nodegit for Node 9.
If you choose to use Node 9, installation of Antora will take considerably longer.

To check which version of Node you have installed, if any, open a terminal and type:

 $ node --version

If this command fails with an error, you don't have Node installed.
If the command doesn't report a Node 8 version (e.g., v{version-node}), you don't have a suitable version of Node installed.

If Node 8 is provided by the package manager for your operating system, we encourage you to install Node that way.
Otherwise, we recommend using {uri-nvm}[nvm] (Node Version Manager) to manage your Node installations.

NOTE: Most CI environments use nvm to install the version of Node used for the build job.
Thus, by using nvm, you closely align your setup with the environment used to generate and publish your production site.

If you're using Linux or macOS, follow {uri-nvm-install}[the nvm installation instructions] to set up nvm on your machine.
If you're using Windows, you can install the {uri-nvm-windows}[Windows port of nvm] via the {uri-choco}[Chocolatey package manager] using `choco install -y nvm`.
Alternatively, you can install the LTS release of Node directly using `choco install -y nodejs-lts`.

Once you've installed nvm, open a *new* terminal and install Node 8 using:

 $ nvm install 8

IMPORTANT: If you're using nvm for Windows, you must enter the full version of Node (i.e., `nvm install {version-node}`) until {uri-nvm-windows}/issues/214[nvm-windows#214] is resolved.

Switch to this version of Node using the following command:

 $ nvm use 8

To make Node 8 the default in new terminals (Linux and macOS only), type:

 $ nvm alias default 8

Now that you have Node installed, you can proceed with installing Antora.

=== Install Antora

To generate a site with Antora, you need the Antora CLI and an Antora site generator pipeline.
Once these packages are installed, you use the `antora` command to generate your site.

We'll begin by installing the Antora CLI using npm.
You should install this package globally so the `antora` command is always available on your PATH.

In your terminal, type:

 $ npm install -g @antora/cli

NOTE: If you prefer Yarn over npm, you can choose to install Antora using `yarn global add @antora/cli`.

Verify the `antora` command is available on your PATH by running:

 $ antora -v

If installation worked, this command should complete successfully and report the version of Antora.

Next, install the default Antora site generator using npm (or Yarn):

 $ npm install -g @antora/site-generator-default

You can opt to install the site generator inside the project containing your playboook file instead of installing it globally.

.Troubleshooting nodegit Installation
[CAUTION]
====
The nodegit dependency may fail on certain Linux distributions with the following error:

....
Error: libcurl-gnutls.so.4: cannot open shared object file: No such file or directory
....

This is an open issue in nodegit.
See https://github.com/nodegit/nodegit/issues/1246[nodegit#1246].

If this happens, you either need to create the missing symlink or force nodegit to be recompiled (instead of using a precompiled binary).

To create the missing symlink, run the following command:

 $ sudo ln -s /usr/lib64/libcurl.so.4 /usr/lib64/libcurl-gnutls.so.4

or, if that fails:

 $ sudo ln -s /usr/lib/libcurl.so.4 /usr/lib/libcurl-gnutls.so.4

Once you've made that symlink, run the `npm install` command again.

 $ npm install -g @antora/site-generator-default

If you aren't comfortable making a system-wide change, you can instead force nodegit to be recompiled instead by passing the `BUILD_ONLY` environment variable to the `npm install` command.

 $ BUILD_ONLY=true npm install -g @antora/site-generator-default

Be aware that recompiling nodegit will make installation take considerably longer.
====

==== Custom Site Generator Pipeline

The `generate` subcommand of `antora` automatically uses the default site generator.
However, Antora is designed with an open architecture to accommodate a myriad of use cases.
That means you can assemble your own site generator pipeline, perhaps to add, substitute, and/or remove components used in the pipeline of the default site generator.
It will be possible in the near future to configure the CLI to use your custom site generator pipeline in place of the default one.

Now that the Antora CLI and default site generator are installed, you are ready to set up a playbook and generate a documentation site.

=== Run Antora to Generate a Site

To generate a site with Antora, you need a playbook file that points to at least one content source repository and a UI bundle.
Since the Antora repository is set up as an Antora documentation project, we can use that for now as our content source.
Antora also provides a default UI for us to use out of the box.

==== Create a Playbook File

First, create a new directory for your site and switch to it.
Next, add a playbook file named [.path]_demo-site.yml_ and populate it with the following contents:

.demo-site.yml
[source,yaml]
----
site:
  title: Antora Docs
content:
  sources:
  - url: https://gitlab.com/antora/antora.git
    branches: master
    start_path: docs
ui:
  bundle: https://gitlab.com/antora/antora-ui-default/-/jobs/artifacts/master/raw/build/ui-bundle.zip?job=bundle-stable
----

Notice we're looking for a documentation component under the [.path]_docs/_ subdirectory of the master branch of the Antora git repository.
We're also using Antora's default UI as the UI for the site.
Antora will take care of assembling all this input together to produce a documentation site.

The UI bundle can be loaded from a URI or a local filesystem path.
If you want to use your own UI bundle, follow the instructions in the README for the https://gitlab.com/antora/antora-ui-default/blob/master/README.adoc[Default UI].

==== Run Antora

To generate the site, you simply point the `antora` command at your playbook file.

In your terminal, type:

 $ antora demo-site.yml

Antora will clone the content repository, convert the AsciiDoc pages to embeddable HTML, wrap the HTML in the page template from the UI, then assemble the pages together with the assets under the destination folder, which defaults to _build/site_.

To view the site, navigate to any HTML page inside the destination folder in your browser.
Using this example, look for the file link:build/site/antora/component-structure.html[].

==== Changing Content

Antora also supports local content, which is essential for authoring.
If you want to make modifications to the documentation, you'll first need to clone the content repository (which in this example just happens to be the Antora repository):

 $ git clone https://gitlab.com/antora/antora.git

Next, update the content source entry in the playbook to point to the local checkout instead of the remote URL:

[source,yaml]
----
content:
  sources:
  - url: antora
    branches: master
    start_path: docs
----

Now, any changes you make to the content under the [.path]_antora/docs/_ folder will be visible the next time you generate the site.

==== Running a Local Server (Optional)

A site generated by Antora is designed to be viewable with or without a web server.
However, you may need to view your site through a web server to test certain features, such as indexified URLs or caching.
You can use the serve package for this purpose.

Install the serve package globally using npm:

 $ npm i -g serve

That puts a command by the same name on your PATH.
Now launch the web server by pointing it at the location of the generated site:

 $ serve build/site

Paste the provided URL into the location bar of your browser and you'll be viewing your site through a local web server.

=== More Information

To learn more about how Antora works, read the article series https://opendevise.com/blog/tag/architecting-antora/[Architecting Antora].

== Getting Help

Antora is designed to help you easily write and publish your documentation.
However, we can't fully realize this goal without your feedback!
We encourage you to report issues, ask questions, share ideas, or discuss other aspects of this project using the communication tools provided below.

=== Issues

*Activity drives progress!*
To that end, the issue tracker is king.

The preferred means of communicating problems, ideas, and other feedback is through the project issue tracker.

* {uri-issues}[Issue tracker] (GitLab)

Any significant change or decision about the project must be logged there.

=== Chat

If you need to switch to real time input, you may be interested in visiting one of the chat rooms.
We've set up two chat rooms for discussing Antora.
Choose the one that best suits your needs.

* {uri-chat-users}[antora/users] (Gitter) -- Community support for Antora users.
* {uri-chat-dev}[antora/dev] (Gitter) -- Discussions involving the development of Antora.

Keep in mind that the discussion logs for these rooms are archived, but there is no guarantee those logs will be saved indefinitely.

=== Social

If you want to share your experience with Antora or help promote it, we encourage you to post about it on social media.
When you talk about Antora on Twitter, you can mention the official account for the project:

* {uri-twitter}[@antoraproject] (Twitter) -- The official Antora account on Twitter.

You can also use the {uri-twitter-hash}[#antora] hashtag to help promote the project or discover other people talking about it.

If you decide you want to get involved to help improve the project, then you'll be interested in the information provided in the <<Contributing>> section.

== Contributing

If you are interested in contributing to this project, please refer to the <<contributing.adoc#,contributing guide>>.
In this guide, you'll learn how to:

* <<contributing.adoc#set-up-workspace,set up your development workspace>>
* <<contributing.adoc#build-project,build the project>>
* <<contributing.adoc#project-rq,submit a merge request>>

Thanks in advance for helping to make this project a success!

== Release Policy

IMPORTANT: The version, roadmap, and support policies described in this section will take affect when the initial stable release of the Antora platform is published.

The Antora platform includes a default site generator package, the packages the default site generator delegates to, and a CLI package.
These packages are released together and follow the https://semver.org[semantic versioning] rules.
Each Antora platform release is versioned *major.minor.maintenance*.

Major::
Major releases occur when new functionality breaks backwards compatibility.
Releases within the same major version number will maintain API compatibility.

Minor::
Minor releases add new features, improvements and fixes and maintain backwards compatibility.

Maintenance::
Maintenance releases patch bugs.

=== Roadmap

The <<roadmap.adoc#,roadmap>> provides the current development direction and schedule for Antora.

Maintenance releases happen as needed depending on the urgency of the fix.
Minor releases typically occur when one or more new features or improvements have been reviewed, tested and approved.

Major releases require architecture, implementation, and QA iterations that are open to community discussion and review.
Major releases will include pre-release versions (major.minor.maintenance-alpha.n | -beta.n | -rc.n).
Pre-release versions will be tagged as _next_ so that the npm client doesn't prefer a pre-release over a stable version.
Once a release candidate (rc) has been thoroughly tested and no new bugs discovered, the stable release will be published.

=== Support

Each major version of the Antora platform is maintained for at least 1 year after the initial public stable release.
Only the latest minor release for each major release will receive maintenance releases.

== Copyright and License

Copyright (C) 2017-2018 OpenDevise Inc. and the Antora Project.

Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
See link:LICENSE[] to find the full license text.

== Authors

Development of Antora is led and sponsored by {uri-opendevise}[OpenDevise Inc].
